from django.contrib import admin
from mysite.models import contact,employee,company

admin.site.register(contact)
admin.site.register(employee)
admin.site.register(company)