from django import forms
from mysite.models import employee
from django.core.exceptions import ValidationError
from django.core.validators import MaxLengthValidator
from django.contrib.auth.models import User

class Student(forms.Form):
    GEN = (('m','male'),('f','female'))

    name = forms.CharField(required=False,widget=forms.TextInput(attrs={'placeholder':'Please Enter Name Here..','class':'bg-dark'}))
    roll_number = forms.IntegerField()
    email = forms.EmailField()
    password = forms.CharField(widget=forms.PasswordInput())
    gender = forms.ChoiceField(choices=GEN)
    dob = forms.DateField(widget = forms.DateInput(attrs={'type':'date'}))

## Validation function
def check_name(value):
    if value[0].lower() != 'a':
        raise ValidationError('Name Must Start with a')
    
class empForm(forms.ModelForm):
    name = forms.CharField(validators=[check_name,MaxLengthValidator(4)])
    class Meta():
        model = employee
        fields = '__all__'
        widgets = {
            'date_of_birth':forms.DateInput(attrs={'type':'date'}),
        }

class registerform(forms.ModelForm):
    confirm_password = forms.CharField(widget=forms.PasswordInput())
    class Meta():
        model = User
        fields=('first_name','username','email','password')
        widgets = {
            'password':forms.PasswordInput()
        }
    def clean(self):
        data = super().clean()
        name = data['first_name']
        pas = data['password']
        cp = data['confirm_password']
        if pas != cp:
            raise ValidationError('Password Does Not Match')
            
        # if name.isalnum():
        #     raise ValidationError('Name should not contain digits')