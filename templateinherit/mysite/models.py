from django.db import models
from django.contrib.auth.models import User

class contact(models.Model):
    name = models.CharField(max_length=200)
    email = models.EmailField(max_length=250)
    message = models.TextField()
    feedback_date = models.DateTimeField(auto_now_add = True)

    def __str__(self):
        return self.name

class employee(models.Model):
    SUB = (('Miss','Miss'),('Mr.','Mr.'),('Mrs.','Mrs.'))
    subtitle = models.CharField(choices=SUB,max_length=100)
    name =models.CharField(max_length=200)
    email = models.EmailField(max_length=200)
    ref_no = models.IntegerField()
    salary = models.DecimalField(max_digits=10,decimal_places=2)
    date_of_birth = models.DateField()
    date_of_joining = models.DateTimeField(auto_now_add = True)

    def __str__(self):
        return self.name


class company(models.Model):
    comp = models.OneToOneField(User,on_delete=models.CASCADE)
    contact = models.IntegerField()
    address = models.TextField()
    logo = models.ImageField(upload_to='images/%Y/%m/%d',blank=True)
    description=models.TextField(blank=True)
    registred_at = models.DateTimeField(auto_now_add=True)
    changed_on = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.comp.username