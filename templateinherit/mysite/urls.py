from django.urls import path
from mysite import views

app_name = 'mysite'

urlpatterns = [
    path('dashboard/',views.about,name='dashboard'),
    path('about/',views.about,name='ab'),
    path('contact/',views.contactView,name='con'),
    path('studentForm/',views.studentForm,name='studentForm'),
    path('employeeForm/',views.empView,name='employeeForm'),
    path('uslogin/',views.uslogin,name='uslogin'),
    path('usregister1/',views.rgUsingMF,name='usregister1'),
    path('usregister/',views.usregister,name='usregister'),
    path('uslogout',views.uslogout,name='logout'),
    path('check_user',views.check_user,name='check_user'),
    path('change_password',views.change_password,name='change_password'),
    path('profile',views.profile,name='profile'),
    path('change_profile',views.change_profile,name='change_profile'),


]