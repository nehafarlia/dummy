from django.shortcuts import render
from mysite.models import contact, company
from mysite.forms import Student,registerform
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib.auth import login,authenticate,logout
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
import datetime
## To create model form
from mysite.forms import empForm
def index(request):
    if 'id' in request.COOKIES:
            id = request.COOKIES['id']
            user = User.objects.get(id=id)
            login(request,user)
            return HttpResponseRedirect('/mysite/dashboard')
    return render(request,'index.html')

@login_required
def home(request):
    data = contact.objects.all()
    return render(request,'home.html',{'data':data})

@login_required
def about(request):
    return render(request,'about.html')

def contactView(request):
    if request.method == 'POST':
        n = request.POST['uname']
        e = request.POST['email']
        m = request.POST['msz']
        data = contact(name=n,email=e,message=m)
        data.save()
        res = 'Hello {}!! Thanks for Your Feedback'.format(n)
        return render(request,'contact.html',{'result':res})    
    return render(request,'contact.html')

def studentForm(request):
    form = Student()
    return render(request,'studentForm.html',{'form':form})

def empView(request):
    myform = empForm()
    if request.method=='POST':
        myform = empForm(request.POST)
        if myform.is_valid():
            myform.save()
            context={'frm':myform,'color':'success','msz':'Saved Successfully!!'}
            return render(request,'employee.html',context)
    return render(request,'employee.html',{'frm':myform})

def rgUsingMF(request):
    form = registerform()
    if request.method=='POST':
        form = registerform(data=request.POST)
        if form.is_valid():
            user=form.save()
            user.set_password(user.password)
            form.save()
            return render(request,'registration.html',{'frm':form,'status':'Registred Successfully!!!'})

    return render(request,'registration.html',{'frm':form}) 
    
def uslogin(request):  
    if request.method=='POST':
        usn = request.POST['username']
        pwd = request.POST['password']
        user = authenticate(username=usn,password=pwd)
        
        if user:
            if user.is_staff:
                login(request,user)
                response = HttpResponseRedirect('/mysite/compdash')
                response.set_cookie('username',usn)
                response.set_cookie('id',user.id)
                response.set_cookie('logintime',datetime.datetime.now())
                return response
            elif user.is_active:
                login(request,user)
                response = HttpResponseRedirect('/mysite/dashboard')
                response.set_cookie('username',usn)
                response.set_cookie('id',user.id)
                response.set_cookie('logintime',datetime.datetime.now())
                return response
            
                return response
        else:
            return render(request,'login.html',{'status':'Invalid Details'})

    return render(request,'login.html')

def usregister(request):
    if request.method=="POST":
        first = request.POST['first_name']
        last = request.POST['last_name']
        un = request.POST['username']
        pwd = request.POST['password']
        em = request.POST['email']
        con = request.POST['contact']
        add = request.POST['address']

        user = User.objects.create_user(un,em,pwd)
        user.first_name = first
        user.last_name = last
        user.is_staff=True
        user.save()
        
        comp = company(comp=user,contact=con,address=add)
        comp.save()
        return render(request,'register.html',{'status':'Success'})
    return render(request,'register.html')

def uslogout(request):
    logout(request)
    response = HttpResponseRedirect('/')
    response.delete_cookie('id')
    return response
 
def check_user(request):
    un = request.GET['username']
    check = User.objects.filter(username=un)
    if len(check)!=0:
        return HttpResponse("A user with this name already exists!!")
    else:
        return HttpResponse("Username Validation Success!!!")
@login_required
def comp_dash(request):
    return render(request,'company_dashboard.html')

from django.contrib.auth.hashers import check_password
@login_required
def change_password(request):
    user = User.objects.get(username=request.user.username)
    login_user_password = request.user.password
    if request.method == "POST":
        current = request.POST["old"]
        newpas = request.POST["new"]

        check = check_password(current,login_user_password)
        if check==True:
            user.set_password(newpas)
            user.save()
            dict ={'status':'Password Changed Successfully!!','col':'success'}
            return render(request,'change_password.html',dict)
        else:
            dict={'status':'Incorrect Current Password','col':'danger'}
            return render(request,'change_password.html',dict)


    return render(request,'change_password.html')

def profile(request):
    user = User.objects.get(username = request.user.username)
    return render(request,'profile.html',{'data':user})

def change_profile(request):
    user = User.objects.get(username = request.user.username)
    if request.method == "POST":
        fs = request.POST['first']
        ls = request.POST['last']
        em = request.POST['email']
        if fs !="" and ls != "" and em !="":
            user.first_name=fs
            user.last_name =ls
            user.email = em
            user.save()
            return render(request,'change_profile.html',{'data':user,'status':'Changes Saved Successfully!!','c':'success'})
        else:
            return render(request,'change_profile.html',{'data':user,'status':'All fields are reuired!!','c':'warning'})

    return render(request,'change_profile.html',{'data':user})